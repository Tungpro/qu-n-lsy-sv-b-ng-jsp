<%-- 
    Document   : student_list
    Created on : 23-Mar-2022, 17:37:15
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
 <head>
	<title>List Student</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

	<!-- Popper JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</head>
    <body>
        <h1 class="text-center">Danh sach</h1>
        <div class="container">
            <div class="panel panel-primary">
                <table class="table table-hover hover-bordered">
                    <tr>
                        <td>STT</td>
                        <td>Ten </td>
                        <td>Tuoi</td>
                        <td>Dia chi</td>
                        <td>Sửa</td>
                        <td>Xóa</td>
                    </tr>
                    <c:forEach var="std" items="${studentList}">
                        <tr>
                        <td>${std.id}</td>
                        <td>${std.fullname} </td>
                        <td>${std.age} </td>
                        <td>${std.address} </td>
                        <td>
                             <a href="?id=${std.id}">
                            <button class="btn btn-warning">Edit</button>
                            </a>
                        </td>
                        <td>
                            <form method="POST" action="StudentDelete">
                                <input type="hidden" name="id" value="${std.id}" />
                                <button type="submit" onclick="confirm('Bạn có muốn xóa')" class="btn btn-danger">Delete</button>
                             </form>
                              
                        </td>
                    </tr>
                    </c:forEach>
                </table>
            </div>
            
            <div class="panel-body">
                <form method="POST">
                    <input type="hidden" name="id" value="${std.id}" />
                    <div class="form-group">
                        <label>Full Name</label>
                        <input class="form-control" type="text" value="${std.fullname}" name="fullname" />
                    </div>
                    <div class="form-group">
                        <label>age</label>
                        <input class="form-control" type="number" name="age" value="${std.age}" />
                    </div>
                      <div class="form-group">
                        <label>Address</label>
                        <input class="form-control" type="text" name="address" value="${std.address}"/>
                    </div>
                    <button type="submit" class="btn btn-success">add</button>
                </form>
            </div>
        </div>
    </body>
</html>
