/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package aptech;

import entity.Student;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class StudentServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/html;charset=UTF-8");
//        try (PrintWriter out = response.getWriter()) {
//            /* TODO output your page here. You may use following sample code. */
//            out.println("<!DOCTYPE html>");
//            out.println("<html>");
//            out.println("<head>");
//            out.println("<title>Servlet StudentServlet</title>");            
//            out.println("</head>");
//            out.println("<body>");
//            out.println("<h1>Servlet StudentServlet at " + request.getContextPath() + "</h1>");
//            out.println("</body>");
//            out.println("</html>");
//        }.
            
           // khaib báo và khởi tạo
           EntityManagerFactory factory = Persistence.createEntityManagerFactory("QuanLySinhVienPU");
           EntityManager em = factory.createEntityManager();
           
           
           // chức năng edit ;
            Student std = em.find(Student.class, 0);
          if(request.getParameter("id") != null){
               int id = Integer.parseInt(request.getParameter("id"));
               std = em.find(Student.class, id);
          }
           
           // lấy danh sách
    
           Query query = em.createNamedQuery("Student.findAll",Student.class);
           List<Student> list = query.getResultList();
           
           // test
           for(Student student : list){
                System.out.println(student.getFullname());
           }
           
           request.setAttribute("std", std); 
           request.setAttribute("studentList", list);
           
          RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/student_list.jsp");
          dispatcher.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String fullname = request.getParameter("fullname").toString();
        int age = Integer.parseInt(request.getParameter("age").toString());
        String address = request.getParameter("address").toString();
        
         int id = Integer.parseInt(request.getParameter("id").toString());
         //int dete = Integer.parseInt(request.getParameter("dete").toString());
         
         EntityManagerFactory factory = Persistence.createEntityManagerFactory("QuanLySinhVienPU");
         EntityManager em = factory.createEntityManager();
         
         
       
         if(id > 0){
            
//            if(clear == 1){
//                // Xóa
//                Student std =  em.find(Student.class, id);
//         
//                // Thực hiện insert
//                em.getTransaction().begin();
//                em.remove(std);
//                em.getTransaction().commit();
//            }else{
                // cập nhật
                Student std =  em.find(Student.class, id);
         
                std.setFullname(fullname);
                std.setAddress(address);
                std.setAge(age);


                // Thực hiện insert
                em.getTransaction().begin();
                em.persist(std);
                em.getTransaction().commit();
//            }
          
           
         }else{
             
            // Thêm mới 
            Student std = new Student();
            
            std.setFullname(fullname);
            std.setAddress(address);
            std.setAge(age);


            // Thực hiện insert
            em.getTransaction().begin();
            em.persist(std);
            em.getTransaction().commit();
         }
          
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
